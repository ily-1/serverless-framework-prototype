package com.serverless;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.AmazonRDSClientBuilder;
import com.amazonaws.services.rds.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

public class Handler implements RequestHandler<S3Event, String> {

	public String handleRequest (S3Event s3event, Context context) {
		try {
			//get files from one bucket and copy it to other
			AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
			S3EventNotification.S3EventNotificationRecord record = s3event.getRecords().get(0);
			String srcKey = record.getS3().getObject().getUrlDecodedKey();
			String srcBucket = record.getS3().getBucket().getName();
			S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
			InputStream objectData = s3Object.getObjectContent();

//			String bucketName = "test-bucket-name-10-11";
//			if (!s3Client.doesBucketExistV2(bucketName)) {
//				s3Client.createBucket(bucketName, Region.EU_Frankfurt);
//				//Thread.sleep(3000);
//				System.out.println("Bucket with name " + bucketName + " was created.");
//			}
//
//			String release = getRelease(srcKey);
//			PutObjectRequest request = new PutObjectRequest(bucketName, release + "/" + srcKey,  objectData, s3Object.getObjectMetadata());
//			s3Client.putObject(request);

			//database
			AmazonRDS amazonRDS = AmazonRDSClientBuilder.standard().withRegion(Regions.EU_CENTRAL_1).build();
			DescribeDBInstancesResult dbInstancesResult = amazonRDS.describeDBInstances();
			List<DBInstance> dbInstanceList = dbInstancesResult.getDBInstances();
			System.out.println("Print all db instances");
			for (DBInstance dbInstance : dbInstanceList) {
				System.out.println("Master user: "+ dbInstance.getMasterUsername());
				System.out.println("DB instance name: " + dbInstance.getDBName());
				System.out.println("------------------------------");
				System.out.println(dbInstance);
				System.out.println("------------------------------");
			}

			if (srcKey.contains("checksums.prop")) {
				Properties properties = new Properties();
				properties.load(objectData);
				for (Map.Entry<Object, Object> property: properties.entrySet()) {
					String fileName = (String) property.getKey();
					String fileChecksum = (String) property.getValue();
					System.out.println("File name: " + fileName + " File checksum: " + fileChecksum);
				}

				Connection connection = getRemoteConnection();
				System.out.println("Connection: " + connection);
				String insertQuery = "INSERT INTO prototype.builds (name, checksum, rel) VALUES (?, ?, ?);";
				PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
				preparedStatement.setString(1, "ACDL_10.11.0.0.125");
				preparedStatement.setString(2, "asdzxczcxzzcxq324eraedad");
				preparedStatement.setString(3, "10.11");
				int insertedRows = preparedStatement.executeUpdate();
				System.out.println("Inserted rows count: " + insertedRows);
			}

		} catch (Exception e) {
			System.out.println("Exception is thrown");
			System.out.println(e.getMessage());
			System.out.println(e.toString());
		}

		return "Ok";
	}

	private String getRelease(String srcKey) {
		String build = srcKey.substring(0, srcKey.indexOf("/"));
		String version = build.substring(build.indexOf("_") + 1);
		String[] versionTokens = version.split("\\.");
		return versionTokens[0] + "." + versionTokens[1];
	}

	private static Connection getRemoteConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String dbName = "prototype";
			String userName = "admin";
			String password = "DataBasePa22";
			String hostname = "builds-prototype.c0ptndrx8whd.eu-central-1.rds.amazonaws.com";
			String port = "3306";

			String jdbcUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbName;

			return DriverManager.getConnection(jdbcUrl, userName, password);
		}
		catch (ClassNotFoundException | SQLException e) {
			System.out.println(e.toString());
		}

		return null;
	}
}
