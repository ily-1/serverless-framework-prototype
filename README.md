# SETUP
1. Install python
2. Install serverless framework ( Execute steps from "Initial Setup" from here: https://www.serverless.com/framework/docs/providers/aws/guide/quick-start/ )
3. Setup AWS credentials to serverless framework with following command exetuted with cmd console.
    serverless config credentials --provider aws –key {keyValue} –secret {secretValue} –profile {userName}
Where keyValue, secretValue, userName are values for AWS IAM user for your account. (If you don't have user see how to add here: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html)
4. Go to root project directory and build project with following command:
    gradlew clean build
5. From root project execute following command to push all functions into AWS account:
    sls deploy -v
6. Open project with IntelliJ. If made some changes and you want to redeploy project into AWS execute following commands:
    1. gradlew clean build
    2. sls deploy -v
Build command is to package changes and second comamnd is to redeploy it.





